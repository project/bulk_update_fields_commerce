# bulk_update_fields_commerce
Drupal 8 module to bulk update fields from one value to another

<strong>Synopsis</strong>
A module for those who need to bulk update fields for commerce_order and commerce_products.

<strong>Similar Projects</strong>
None known

<strong>Requirements</strong>
Latest release of Drupal 8.x.

<strong>Configuration</strong>
Enable the module
Go to commerce order or product overview page
Select orders or products
Select Action "Bulk Update Fields to Another Value"
Hit "Apply to selected items"
Select the Fields to alter (NOTE: ok to select fields not in all content types)
Hit "Next"
Update the fields with appropriate values (NOTE dates not working yet)
Hit "Next"
"Are you sure?" - Hit "Alter Fields"
Wait for batch process.
Done
